﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace RoutineEngine
{

       
     public class Deserial {
         public void DeserializeObject(string filename){

            Console.WriteLine("Reading with XmlReader");

            try
            {

                XmlSerializer serializer = new
                XmlSerializer(typeof(Agend), new XmlRootAttribute("xml"));

                FileStream fs = new FileStream(filename, FileMode.Open);

                Agend i;
                i = (Agend)serializer.Deserialize(fs);
                fs.Close();


            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }

        }
    }
}
