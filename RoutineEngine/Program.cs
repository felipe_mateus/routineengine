﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topshelf;
namespace RoutineEngine
{
    class Program
    {
        static void Main(string[] args) {
            var exitCode = HostFactory.Run(x =>
            {
                x.Service<Engine>(s =>
                {
                    s.ConstructUsing(engine => new Engine());
                    s.WhenStarted(engine => engine.Start());
                    s.WhenStopped(engine => engine.Stop());
                });

                x.RunAsLocalSystem();
                x.SetServiceName("RoutineEngine");
                x.SetDisplayName("Routine Engine");
                x.SetDescription("Engine for routines");

            });

            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
        }
    }
}
