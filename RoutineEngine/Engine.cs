﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace RoutineEngine
{
    public class Engine {

        private readonly Timer _timer;

        public Engine() {
            _timer = new Timer(1000) { AutoReset = true };
            _timer.Elapsed += Timer_Elapsed;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            string[] lines = new string[] { DateTime.Now.ToString()};
            //File.AppendAllLines(@"C:\TESTES\engineoperation.log", lines);
        }

        public void Start() {
            _timer.Start();
            Deserial deserial = new Deserial();
            deserial.DeserializeObject("C:\\Dados\\xml.xml");
            Exec exec = new Exec();
            exec.proc("notepad.exe","");


        }

        public void Stop() {
            _timer.Stop();
        }

    }
}
