﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RoutineEngine
{
    public class RegistrationInfo
    {
        //Variáveis de resgistro dos clientes
        [XmlElement("Date")]
        public DateTime Date { get; set; }

        [System.Xml.Serialization.XmlElement("Author")]
        public String Author { get; set; }

        [System.Xml.Serialization.XmlElement("Client")]
        public String Client { get; set; }

        [System.Xml.Serialization.XmlElement("Code")]
        public String Code { get; set; }

    }

    public class Act
    {
        public DateTime StartBoundary { get; set; }
        public String By { get; set; }
        public String Interval { get; set; }
        public String Routine { get; set; }
        public Boolean Reset { get; set; }

    }

    [XmlRoot("xml")]
    public class Agend
    {
        public RegistrationInfo RegistrationInfo { get; set; }

        [XmlArray("Config")]
        public List<Act> Act { get; set; }

    }
}


